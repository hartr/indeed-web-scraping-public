"""
Scrape jobs on Indeed, find compatible jobs using ChatGPT, and save jobs to
apply to in a CSV. This script (run_assistant.py) is the main file to run
to find jobs.

Note that run_assitant is a bit slow as I have put sleep commands throughout
in an attempt to give selenium driver time to load and to try to avoid
CAPTCHA checkpoints.

To run these scripts, first complete the `job_search_config.json`. This
contains all the settings to search for jobs. You can see what settings are
available, and what defaults are provided, by checking the `JobSearchParams`
and `RunSettings` dataclasses in the `utils` module.

There are up to two environment variables to set:
CONFIG_PATH - optional - defaults to '/job-application-assistant/job_search_config.json
OPENAI_KEY - optional - only required if using OpenAI, go to their site to get a key

To run the scripts:
1. Open a terminal and activate your Conda or pip environment
2. Change directory to `path/to/job-application-assistant/src`
3. Run `python run_assistant.py` or open up your IDE and run `run_assistant`

The following process occurs when running `run_assistant`
1. Load job_search_config.json
2. Create or open existing job CSVs. There are two CSVs (RecordDF) that
    are saved. One CSV that contains all jobs processed (defaults to
    processed_jobs.csv). If the job info was successfully scraped and
    analyzed then it will be saved to the processed jobs CSV so it won't
    be processed again. The other CSV contains your "saved jobs" (defaults
    to saved_jobs.csv), jobs that these scripts have determined you are
    qualified for and that are likely worth applying to.
3. OPTIONAL - read in your resume to be used to check if applicant is
    qualified for the job.
4. Load nltk models to check for job descriptions
5. OPTIONAL - check if any saved job has expired, if so then remove job
    from saved jobs.
6. Outer loop searches through each job location
7. Inner loop search for the job title on Indeed
8. Navigate Indeed and search for job parameters. If something goes wrong
    user has the option to search for next job title.
9. Scrape and analyze job information. Save the job to the saved jobs CSV
    if you're qualified and always save the job to the processed jobs CSV.
    If something goes wrong user has the option to retry search from page
    error occurred.
10. Overwrite the CSVs with the new jobs
"""
# TODO: Developer notes:
# TODO: look into color coding logs
# TODO: add basic precommit hooks
# TODO: look into bug that prevents first card on page from being processed
# TODO: use last date saved in saved_jobs as way to automate settings
# TODO: make copies of saved_jobs each time scripts are run
# TODO: fix issue where ChatGPT request gets hung up
import logging
from selenium import webdriver

from process_job import analyze_jobs
from navigation import search, check_expired_job_posts
from classes import RecordDF
from utils import (
    open_txt,
    process_config,
    TextProcessingUtils,
    EXPECTED_COLUMNS_SAVED,
    EXPECTED_COLUMNS_PROCESSED,
    SORT_BY_COLS,
    LOG_LEVEL
)

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    job_search_params, run_settings = process_config()
    saved_rdf = RecordDF(run_settings.saved_jobs_path, EXPECTED_COLUMNS_SAVED, parse_dates=SORT_BY_COLS)
    processed_rdf = RecordDF(run_settings.processed_jobs_path, EXPECTED_COLUMNS_PROCESSED, parse_dates=SORT_BY_COLS)
    resume = open_txt(run_settings.resume_path)

    # Models need to be loaded for utils.advanced_check_for_saved_job
    TextProcessingUtils.load_nltk_models()

    logger.info('Starting up driver')
    driver = webdriver.Firefox()

    if run_settings.remove_expired_jobs:
        logger.info('Checking saved jobs CSV for expired job posts')
        check_expired_job_posts(driver, saved_rdf)

    driver.get('https://indeed.com')

    try:
        # Loop through each search location
        for location in job_search_params.location:

            # Loop through all the job titles to search for
            for job_title in job_search_params.search_jobs:

                analyze_jobs_result = ''
                page = job_search_params.page

                while analyze_jobs_result != 'success':

                    logger.info(f'Searching {job_title} jobs in {location} on page {page}')
                    search_result = search(
                        driver,
                        job_title,
                        location,
                        job_search_params.job_posting_age,
                        job_search_params.distance,
                        job_search_params.remote,
                        page
                    )

                    if search_result == "skip":
                        logger.info('Skipping to next search with new job title and/or job location')
                        analyze_jobs_result = 'success'
                        continue

                    analyze_jobs_result = analyze_jobs(
                        driver,
                        job_search_params,
                        run_settings,
                        saved_rdf,
                        processed_rdf,
                        resume=resume
                    )

                    if isinstance(analyze_jobs_result, int):
                        page = analyze_jobs_result

        logger.info(
            f'run_assistant finished successfully! Saved {saved_rdf.new_job_count} new '
            f'jobs to apply to and processed {processed_rdf.new_job_count} new job posts.'
        )

    except Exception as e:
        msg = f" - {e.msg}" if hasattr(e, "msg") else ''
        logger.error(
            f'Exception raised when searching for jobs. CSVs '
            f'will still be saved. Exception: {e.args}{msg}'
        )

    # Sort records by date the jobs were saved
    saved_rdf.sort_by_column(SORT_BY_COLS)
    processed_rdf.sort_by_column(SORT_BY_COLS)
    saved_rdf.save_csv()
    processed_rdf.save_csv()

    driver.quit()
