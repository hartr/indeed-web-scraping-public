"""Constants, miscellaneous functions, and dataclasses."""
import json
import os
import time
import random
import logging
import string
import nltk
import pandas as pd
from datetime import datetime, timedelta
from dataclasses import dataclass
from enum import Enum
from typing import Optional, List, Literal, Tuple, Union
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
from sklearn.metrics.pairwise import cosine_similarity
from collections import Counter

LOG_LEVEL = os.getenv('LOG_LEVEL', logging.INFO)  # LOG_LEVEL must be int, e.g. logging.INFO = 20

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)

BASE_PATH = os.path.abspath(os.path.dirname(__name__))
WEB_DRIVER_WAIT_SEC = 10

# Column names in saved_rdf and processed_rdf
JOB_COMPANY = 'Company'
JOB_TITLE = 'Title'
JOB_URL = 'URL'
JOB_DESCRIPTION = 'Description'
JOB_LANGUAGES = 'Languages'
JOB_DATE_SAVED = 'Date Saved'
JOB_APPLIED_DATE = 'Applied On'
JOB_STATUS = 'Status'
JOB_RESPONSIBILITIES = 'Job Responsibilites'
JOB_NOTES = 'Notes'
JOB_SKIP_REASON = "Skip Reason"

# Column names expected in saved_rdf and processed_rdf
EXPECTED_COLUMNS_SAVED = [
    JOB_DATE_SAVED,
    JOB_STATUS,
    JOB_APPLIED_DATE,
    JOB_COMPANY,
    JOB_TITLE,
    JOB_URL,
    JOB_RESPONSIBILITIES,
    JOB_LANGUAGES,
    JOB_DESCRIPTION,
    JOB_NOTES,
]
EXPECTED_COLUMNS_PROCESSED = [
    JOB_DATE_SAVED,
    JOB_COMPANY,
    JOB_TITLE,
    JOB_URL,
    JOB_SKIP_REASON
]
# Sort the saved_jobs.csv by date saved
SORT_BY_COLS = [JOB_DATE_SAVED]


def wait(sec: int = 1):
    time.sleep(sec + random.random())


def open_txt(path: Optional[str]) -> Optional[str]:
    if path:
        with open(path, "r") as file:
            contents = file.read()
        return contents
    else:
        return None


@dataclass
class Requirement(Enum):
    ANY = 'any'
    ALL = 'all'


@dataclass
class JobSearchParams:
    """Parameters used to search for jobs."""
    search_jobs: Union[str, List[str]]  # job titles to search for
    location: Union[str, List[str]]  # can be a zipcode or city, state
    # Number days back to look for jobs, if Automate use the last time saved_jobs.csv was modified
    # to chose how far back to search for jobs
    job_posting_age: Literal[
        "Last 24 hours",
        "Last 3 days",
        "Last 7 days",
        "Last 14 days",
        "Automate"
    ] = "Automate"
    # Distance from `location`
    distance: Literal[
        "Exact location only",
        "Within 5 miles",
        "Within 10 miles",
        "Within 15 miles",
        "Within 25 miles",
        "Within 35 miles",
        "Within 50 miles",
        "Within 100 miles"
    ] = "Within 35 miles"
    remote: bool = False  # search for remote positions only
    min_years: int = 0  # minimum job experience in years
    max_years: int = 1000  # maximum job experience in years
    # avoid specific words in the job title, letter case does not matter
    avoid_titles: Optional[List[str]] = None
    # require specific words in the job description, case-sensitive!
    required_keywords: Optional[List[str]] = None
    page: int = 1  # desired page number to search from

    def __post_init__(self):
        """Convert parameters to lists to work with run_assistant."""
        if not isinstance(self.search_jobs, list):
            self.search_jobs = [self.search_jobs]
        if not isinstance(self.location, list):
            self.location = [self.location]

    def set_job_posting_age(self, mod_time):
        """Set job posting age based on when saved_jobs.csv was saved."""
        now = int(time.time())
        if mod_time is None:
            job_posting_age = "Last 14 days"  # if no saved_jobs.csv exists
        elif now - mod_time < 60*60*24:
            job_posting_age = "Last 24 hours"
        elif now - mod_time < 60*60*24*3:
            job_posting_age = "Last 3 days"
        elif now - mod_time < 60*60*24*7:
            job_posting_age = "Last 7 days"
        else:
            job_posting_age = "Last 14 days"
        logger.info(f"Automated setting to check {job_posting_age} for jobs")
        self.job_posting_age = job_posting_age


@dataclass
class RunSettings:
    """Parameters used to use run_assistant.py."""
    # default path to save (or open) saved jobs
    saved_jobs_path: str = os.path.join(BASE_PATH, '..', 'saved_jobs.csv')
    # default path to save (or open) processed jobs
    processed_jobs_path: str = os.path.join(BASE_PATH, '..', 'processed_jobs.csv')
    # whether to remove job postings that have expired from saved jobs CSV, if Automate use the
    # last time saved_jobs.csv was modified to chose whether to remove jobs
    remove_expired_jobs: Union[bool, Literal["Automate"]] = "Automate"
    # path to your resume, must be a .txt file
    resume_path: Optional[str] = None
    # whether to use ChatGPT, requires OPENAI_KEY env variable
    use_chatgpt: bool = False
    # whether to use ChatGPT to identify software tools
    identify_software_tools: bool = False

    def __post_init__(self):
        """Check different required conditions for RunSettings."""

        if self.identify_software_tools and not self.use_chatgpt:
            raise ValueError('use_chatgpt must be set to True if identify_software_tools is True')

        if self.resume_path and not self.use_chatgpt:
            raise ValueError('use_chatgpt must be set to True if specifying a resume_path')

        if self.resume_path and not self.resume_path.endswith('.txt'):
            raise ValueError('resume_path must be None or a string that ends with ".txt"')

    def get_modification_time(self) -> Optional[int]:
        """Get time file last modified given a file path."""
        if os.path.exists(self.saved_jobs_path):
            return int(os.path.getmtime(self.saved_jobs_path))
        else:
            return None

    def set_remove_expired_jobs(self, mod_time: int):
        """Set class attribute based on mod_time."""
        # Do not check for expired jobs if saved job CSV was modified recently
        if mod_time is None:  # if no saved_jobs.csv exists
            self.remove_expired_jobs = False
        else:
            now = time.time()
            num_sec_in_half_day = 60*60*12
            recent_saved_jobs = now - mod_time < num_sec_in_half_day
            self.remove_expired_jobs = False if recent_saved_jobs else True
        logger.info(f"Automated setting remove_expired_jobs set to {self.remove_expired_jobs}")


def user_fixes_driver_failure(e: Exception, function_name: str) -> str:
    """Get input from user after issue running driver."""
    user_input = input(
        f'Could not find element from function {function_name}. Your options include: \n1. If a '
        f'CAPTCHA checkpoint occurs, get past checkpoint then press enter\n2. If a 502 error code '
        f'appears or a single job appears on your page, type `retry` then press enter\n3. If no '
        f'error appears to have occurred, press enter\n4. If you would rather quit the application '
        f'and save the CSVs, type `exit` then press enter\n5. If you would rather skip to the next '
        f'search, type `skip` then press enter'
    )
    if user_input == 'exit':
        raise RuntimeError(
            'user exit from run_assitant'
        ) from e
    return user_input


def process_config() -> Tuple[JobSearchParams, RunSettings]:
    """Load settings to run scripts."""
    config_path = os.getenv('CONFIG_PATH', None)
    if not config_path:
        config_path = os.path.join(BASE_PATH, '..', 'job_search_config.json')
    logger.info(f'Loading config file from {config_path}')
    with open(config_path, 'r') as f:
        config_dict = json.load(f)
    job_search_params = JobSearchParams(**config_dict['job_search_params'])
    run_settings_dict = config_dict['run_settings'] if 'run_settings' in config_dict else {}
    run_settings = RunSettings(**run_settings_dict)

    # Set the automated settings
    mod_time = run_settings.get_modification_time()
    if job_search_params.job_posting_age == "Automate":
        job_search_params.set_job_posting_age(mod_time)
    if run_settings.remove_expired_jobs == "Automate":
        run_settings.set_remove_expired_jobs(mod_time)

    return job_search_params, run_settings


class TextProcessingUtils:

    @staticmethod
    def load_nltk_models():
        """
        Load nltk models to processes text -- used to see if job
        descriptions are similar.
        """
        nltk.download("punkt")
        nltk.download("wordnet")

    @staticmethod
    def preprocess_text(text: str) -> List[str]:
        """Preprocess text and get tokens."""
        text = text.lower()
        text = text.translate(str.maketrans("", "", string.punctuation))
        tokens = word_tokenize(text)
        return tokens

    @staticmethod
    def get_synonyms(word: str) -> List[str]:
        """
        Get the lemma for each synonym of `word`.

        Helpful info:
        - A synset (short for "synonym set") refers to a group of words or
        expressions that are considered synonymous, or closely related in
        meaning.
        - A lemma refers to the base or dictionary form of a word. E.g.
        running -> run, mice -> mouse.
        """
        synonyms = []
        for syn in wordnet.synsets(word):
            for lemma in syn.lemmas():
                synonyms.append(lemma.name().lower())
            break
        return synonyms

    @staticmethod
    def replace_synonyms(tokens: List[str]) -> List[str]:
        """Replace tokens with a synonym/lemma that is more common."""
        replaced_tokens = []
        for token in tokens:
            synonyms = TextProcessingUtils.get_synonyms(token)
            if synonyms:
                # Note: Maybe select most common (highest count) synonym?
                replaced_tokens.append(synonyms[0])  # Replace with the first synonym
            else:
                replaced_tokens.append(token)
        return replaced_tokens

    @staticmethod
    def calculate_cosine_similarity(vec1: List[int], vec2: List[int]) -> int:
        return cosine_similarity([vec1], [vec2])[0][0]


def basic_check_for_processed_job(df: pd.DataFrame, url: str, title: str) -> bool:
    """Check if part of job URL and job title exists in processed_jobs."""
    url_length = 180
    for i in range(len(df)):
        if url[:url_length] in df.loc[i, JOB_URL] and title == df.loc[i, JOB_TITLE]:
            return True
    return False


def advanced_check_for_saved_job(df: pd.DataFrame, company: str, title: str, description: str) -> bool:
    """
    Check if description is similar to existing saved_jobs with same job
    title and company name.
    """

    # First check if there is a row(s) in the dataframe where the company and title match
    matching_df = df[(df[JOB_COMPANY] == company) & (df[JOB_TITLE] == title)]

    # Job title and company were not found in processed_jobs.csv
    if len(matching_df) == 0:
        return False

    # Loop through each job with same company and title
    for i in list(matching_df.index):
        new_job_description = description
        processed_job_description = df.loc[i, JOB_DESCRIPTION]

        # Get descriptions into a state that's easier to analyze
        tokens1 = TextProcessingUtils.preprocess_text(new_job_description)
        tokens2 = TextProcessingUtils.preprocess_text(processed_job_description)

        # Replace tokens with a synonym/lemma that is more common
        replaced_tokens1 = TextProcessingUtils.replace_synonyms(tokens1)
        replaced_tokens2 = TextProcessingUtils.replace_synonyms(tokens2)

        # Create word frequency vectors
        word_freq1 = Counter(replaced_tokens1)
        word_freq2 = Counter(replaced_tokens2)

        # Get unique words from each description
        unique_words = list(set(replaced_tokens1 + replaced_tokens2))

        # Create vectors using the count of the word frequency for all of the unique words
        vector1 = [word_freq1[word] for word in unique_words]
        vector2 = [word_freq2[word] for word in unique_words]

        similarity = TextProcessingUtils.calculate_cosine_similarity(vector1, vector2)

        # If the job descriptions are similar and the last time the similar job
        # was saved was within 28 days
        if similarity > 0.95 and df.loc[i, JOB_DATE_SAVED] > datetime.now() - timedelta(days=28):
            return True

    # Return False if job hasn't been saved and new job post processing should continue
    return False
