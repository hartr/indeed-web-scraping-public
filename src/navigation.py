"""
Navigate through web pages using Firefox and Selenium.

There are two different ways to get a WebElement:
1. jdAge = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.CLASS_NAME, 'yosegi-FilterPill-pillLabel')))
2. searchButton = driver.find_element(By.CLASS_NAME, 'yosegi-InlineWhatWhere-primaryButton')
The first is used when needing to wait for the page to load before clicking, entering data, etc.
"""
import logging
import pandas as pd
from typing import Optional
from random import randint
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from utils import (
    wait,
    user_fixes_driver_failure,
    JOB_APPLIED_DATE,
    JOB_URL,
    WEB_DRIVER_WAIT_SEC,
    LOG_LEVEL
)
from classes import RecordDF

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)


def search(
        driver: WebDriver,
        search_for: str,
        location: str,
        job_posting_age: str,
        distance: str,
        remote: bool,
        page: int
) -> Optional[str]:
    """
    Search for job using job title, location, distance, etc.

    Args:
        driver: Selenium Firefox WebDriver
        search_for: job title to search for
        location: location to search for job
        job_posting_age: filter for age of job posts
        distance: filter for distance of jobs from location
        remote: filter for remote jobs
        page: job page to start from

    Returns:
        None: if function completes without issue
        '': if user corrects filters and finishes search
        'skip': if the user enters 'skip' as an input
    """
    # Add job description to search for
    search_text = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.ID, 'text-input-what')))
    wait(4)
    search_text.clear()
    search_text.send_keys(search_for)
    wait()

    # Add location to search for
    where = driver.find_element(By.ID, 'text-input-where')
    where.clear()
    where.send_keys(location)
    wait()

    # Search for jobs
    try:
        # "Search" button occurs on initial search page
        search_button = driver.find_element(By.XPATH, '//button[contains(text(), "Search")]')
    except NoSuchElementException:
        # "Find Jobs" buttons occurs when a single job post is viewed
        search_button = driver.find_element(By.XPATH, '//button[contains(text(), "Find Jobs")]')
    search_button.click()
    wait()

    # If there is a prompt box that asks for you to sign in, try to exit out of it
    try:
        login_exit = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(@id, 'passport-modal-overlay-social-onetap-modal-close-button')]")))
        login_exit.click()
        wait()
        logger.info('Exited out of login prompt')
    except TimeoutException:
        pass

    try:

        # Click on "Date posted" dropdown
        jd_age = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(@id, 'filter-dateposted')]")))
        jd_age.click()
        wait()

        # Set the age of job posting
        jd_age = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.LINK_TEXT, job_posting_age)))
        jd_age.click()
        wait()

        # Click on "Distance" dropdown
        jd_distance = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(@id, 'filter-radius')]")))
        jd_distance.click()
        wait()

        # Set the distance of job posting
        jd_distance = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.LINK_TEXT, distance)))
        jd_distance.click()
        wait()

        if remote:
            # Click on "Distance" dropdown
            remote_position = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(@id, 'filter-remotejob')]")))
            remote_position.click()
            wait()

            # Set the distance of job posting
            remote_position = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.XPATH, "//a[contains(@class, 'yosegi-FilterPill-dropdownListItemLink') and contains(text(), 'Remote')]")))
            remote_position.click()
            wait()

        # User can specify page to go to or tell program to retry from a certain page if issue occurred
        if page > 1:
            current_page = 1
            while current_page < page:
                next_page = current_page + 1
                next_page_element = driver.find_element(By.XPATH, "//nav[contains(@class, 'css-jbuxu0 ecydgvn0')]").find_element(By.LINK_TEXT, str(next_page))
                next_page_element.click()
                logger.info(f'Navigated to page {next_page}')
                current_page = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'a[data-testid="pagination-page-current"]')))
                current_page = int(current_page.text)

        return None

    except TimeoutException:
        logger.error('Failed to search for different filters.')
        user_input = input(
            'Finish search - Click desired job age, job distance, if remote position, desired page '
            'number, and then press Enter. If filter does not exist, enter "skip" to skip to next '
            'job search.'
        )
        return user_input


def check_expired_job_posts(driver: WebDriver, saved_rdf: RecordDF):
    """
    Check each saved job to see if the job posting has expired. If it has
    expired then remove the saved job from the list of saved jobs.
    """
    drop_rows = []
    for i in range(len(saved_rdf.df)):

        # Don't remove job if applied
        if saved_rdf.df.loc[i, JOB_APPLIED_DATE] != '' and not pd.isna(saved_rdf.df.loc[i, JOB_APPLIED_DATE]):
            continue

        wait(randint(0, 2))  # wait random seconds in an attempt to avoid CAPTCHA checks
        driver.get(saved_rdf.df.loc[i, JOB_URL])  # go to job page

        # Try to check for the text box that says the job posting is expired. If the text box
        # doesn't exist then `break`. If a CAPTCHA checkpoint appears then get user input.
        while True:

            try:
                page_has_loaded = WebDriverWait(driver, WEB_DRIVER_WAIT_SEC).until(EC.element_to_be_clickable((By.CLASS_NAME, 'mosaic-reportcontent-wrapper button')))
                expired_job = driver.find_element(By.XPATH, "//div[contains(@class, 'css-7vgfue eofpmnx0')]")
                if "This job has expired on Indeed" in expired_job.text:
                    drop_rows.append(i)
                    break

            except NoSuchElementException:
                break

            except Exception as e:
                user_input = user_fixes_driver_failure(e, 'check_expired_job_posts')
                if user_input == "skip":
                    logger.info(f'Job url skipped: {saved_rdf.df.loc[i, JOB_URL]}')
                    break
                continue

    if drop_rows:
        logger.info(f'Removing {len(drop_rows)} expired job postings and overwritting saved jobs CSV')
        saved_rdf.df.drop(drop_rows, inplace=True)
        saved_rdf.df.reset_index(drop=True, inplace=True)
        saved_rdf.save_csv()
