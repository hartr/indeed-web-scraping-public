"""This script was a POC and could be improved."""
import logging
import time
from utils import open_txt, process_config, LOG_LEVEL
from chatgpt import ChatGPTClient, ChatCompletionMessage

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)

NUM_BULLETS = 8


def ask_chatgpt_to_create_resume_content():
    question = (
        f"Given this job description and the bullet points from my resume, create new resume bullet "
        f"points that use my past experience to show I have the skills needed for the job. Give me "
        f"{NUM_BULLETS} new bullet points.\n\nMy resume is: {resume}\n\nThe job description is: {job_description}"

    )
    messages = chatgpt.message_constructor()
    messages.append(ChatCompletionMessage(role="user", content=question))
    response = chatgpt.create_chat_completion(messages)
    return response


if __name__ == '__main__':
    chatgpt = ChatGPTClient()
    job_search_params, run_settings = process_config()
    resume = open_txt(run_settings.resume_path)

    logger.info('Input a job description and ChatGPT will take your resume and create new resume bullet points')
    job_description = input(
        'Enter job description as a single line (no new line breaks) or type `exit` to quit process.'
    )

    while job_description != 'exit':
        # Input just the job description and don't include things like the company desciription,
        # equal employment opportunity info, and other info not related to the job responsibilities.
        response = ask_chatgpt_to_create_resume_content()
        logger.info(response)
        time.sleep(2)
        job_description = input(
            'Enter a new job description as a single line (no new line breaks) or type `exit` to quit process.'
        )
