"""OpenAI ChatGPT client that makes HTTP requests to ChatGPT."""
import requests
import os
import logging
from copy import deepcopy
from abc import ABC
from typing import List, Optional, Union
from dataclasses import dataclass
from datetime import datetime
from requests.exceptions import HTTPError

from utils import wait, LOG_LEVEL

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)


@dataclass
class ChatCompletionMessage:
    # The role of the messages author. One of system, user, assistant, or function.
    role: str

    # The contents of the message. content is required for all messages except assistant messages
    # with function calls.
    content: Optional[str]

    # The name of the author of this message. name is required if role is function,
    # and it should be the name of the function whose response is in the content.
    # May contain a-z, A-Z, 0-9, and underscores, with a maximum length of 64 characters.
    name: Optional[str] = None

    # The name and arguments of a function that should be called, as generated by the model.
    function_call: Optional[object] = None

    def to_dict(self):
        message = vars(self)
        message = {k: v for k, v in message.items() if v is not None}
        return message


class ChatGPTRequestHandler(ABC):
    def __init__(self, env_var: str = 'OPENAI_KEY'):
        self.base_url = 'https://api.openai.com/v1'
        self.header_content_type = {"Content-Type": "application/json"}
        self.header_accept = {"Accept": "application/json"}
        key = os.getenv(env_var, None)
        if not key:
            raise RuntimeError(f'env_var environment variable is not set')
        self.header_auth = {"Authorization": f"Bearer {key}"}
        self.headers = {**self.header_auth, **self.header_accept}
        self.retries = 0

    def _raise_for_status(self, response):
        try:
            response.raise_for_status()
        except HTTPError as e:
            raise HTTPError(f'Unexpected response: {e.args}, response.json(): {response.json()}')

    def request(
            self,
            request_method: str,
            endpoint: str,
            request_body: Optional[dict] = None,
            headers: Optional[dict] = None,
            retry_on_502_or_503: bool = True,
            time_to_wait: int = 3 * 60,
            num_retries: int = 2
    ) -> dict:

        if not headers:
            headers = self.headers

        if request_method == 'GET':
            response = requests.get(self.base_url + endpoint, headers=headers)

        elif request_method == 'POST':
            if 'Content-Type' not in headers:
                headers = {**headers, **self.header_content_type}
            response = requests.post(self.base_url + endpoint, json=request_body, headers=headers)

            # Sometimes too many requests can be sent in a period of time, wait a little bit
            if retry_on_502_or_503 and response.status_code in [502, 503] and self.retries <= num_retries:
                self.retries += 1
                logger.warning(
                    f'{response.status_code} ChatGPT response. Waiting {time_to_wait} '
                    f'seconds. Time now: {datetime.now().strftime("%H:%M:%S")}'
                )
                wait(time_to_wait)
                response = requests.post(self.base_url + endpoint, json=request_body, headers=headers)

        else:
            raise ValueError(f'request_method {request_method} is not accepted')

        self._raise_for_status(response)
        response_json = response.json()
        logger.info(f"Tokens used for request: {response_json['usage']}")
        return response_json


class ChatGPTClient(ChatGPTRequestHandler):
    """
    There are two ways to use ChatGPT, either using HTTP requests or
    using the openai package. I don't have a strong reason to do it
    one way or another.

    Using HTTP requests pros:
    - don't have to install openai package

    Using openai package pros:
    - perhaps better error messages
    - slightly less code

    Model endpoint compatibility:
    - /v1/chat/completions	gpt-4, gpt-4-0613, gpt-4-32k, gpt-4-32k-0613,
        gpt-3.5-turbo, gpt-3.5-turbo-0613, gpt-3.5-turbo-16k,
        gpt-3.5-turbo-16k-0613
    - /v1/completions	text-davinci-003, text-davinci-002, text-curie-001,
        text-babbage-001, text-ada-001
    - /v1/edits	text-davinci-edit-001, code-davinci-edit-001
    - /v1/audio/transcriptions	whisper-1
    - /v1/audio/translations	whisper-1
    - /v1/fine-tunes	davinci, curie, babbage, ada
    - /v1/embeddings	text-embedding-ada-002, text-search-ada-doc-001
    - /v1/moderations	text-moderation-stable, text-moderation-latest
    """
    def __init__(self, env_var: str = 'OPENAI_KEY'):
        super().__init__(env_var)

    def message_constructor(self) -> List[ChatCompletionMessage]:
        """First message to 'initialize' ChatGPT if using create_chat_completion."""
        messages = [ChatCompletionMessage(role="system", content="You are a helpful assistant.")]
        return deepcopy(messages)

    def list_models(self) -> List[dict]:
        """
        Lists the currently available models, and provides basic
        information about each one such as the owner and availability.

        It is possible to use the openai package to list models:

        import os
        import openai
        openai.api_key = os.getenv("OPENAI_API_KEY")
        openai.Model.list()
        """
        response_json = self.request('GET', '/models')
        return response_json['data']

    def retrieve_model(self, model: str = 'gpt-3.5-turbo') -> dict:
        """
        Retrieves a model instance, providing basic information
        about the model such as the owner and permissioning.

        It is possible to use the openai package to retrieve model:

        import os
        import openai
        openai.api_key = os.getenv("OPENAI_API_KEY")
        openai.Model.retrieve("text-davinci-003")
        """
        return self.request('GET', f'/models/{model}')

    def create_chat_completion(
            self,
            messages: List[ChatCompletionMessage],
            model: str = 'gpt-3.5-turbo',
            temperature: float = 1.0,
            n: int = 1,
            stop: Optional[int] = None,
            max_tokens: Optional[int] = None,  # openai by default sets this to inf
            presence_penalty: float = 0.0,
            frequency_penalty: float = 0.0
    ) -> str:
        """
        Creates a model response for the given chat conversation.

        For examples of using this endpoint see:
        https://github.com/openai/openai-cookbook/blob/main/examples/How_to_format_inputs_to_ChatGPT_models.ipynb

        It is possible to use the openai package to use chat completion:

        import os
        import openai
        openai.api_key = os.getenv("OPENAI_API_KEY")
        completion = openai.ChatCompletion.create(
          model="gpt-3.5-turbo",
          messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": "Hello!"}
          ]
        )
        print(completion.choices[0].message)

        Example request body:
        {
          "model": "gpt-3.5-turbo",
          "messages": [
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": "Hello!"}
          ]
        }

        Example response:
        {
          "id": "chatcmpl-123",
          "object": "chat.completion",
          "created": 1677652288,
          "choices": [{
            "index": 0,
            "message": {
              "role": "assistant",
              "content": "\n\nHello there, how may I assist you today?",
            },
            "finish_reason": "stop"
          }],
          "usage": {
            "prompt_tokens": 9,
            "completion_tokens": 12,
            "total_tokens": 21
          }
        }

        Args:
            messages: A list of messages comprising the conversation so far.
            model: ID of the model to use. See the model endpoint
                compatibility table for details on which models work with
                the Chat API.
            temperature: What sampling temperature to use, between 0 and 2.
                Higher values like 0.8 will make the output more random,
                while lower values like 0.2 will make it more focused and
                deterministic.We generally recommend altering this or top_p
                but not both.
            n: How many chat completion choices to generate for each input
                message.
            stop: Up to 4 sequences where the API will stop generating
                further tokens.
            max_tokens: The maximum number of tokens to generate in the
                chat completion. The total length of input tokens and
                generated tokens is limited by the model's context length.
            presence_penalty: Number between -2.0 and 2.0. Positive values
                penalize new tokens based on whether they appear in the
                text so far, increasing the model's likelihood to talk
                about new topics.
            frequency_penalty: Number between -2.0 and 2.0. Positive values
                penalize new tokens based on their existing frequency in
                the text so far, decreasing the model's likelihood to
                repeat the same line verbatim.

        Returns:
            model_chat_completion: response from ChatGPT completing chat
        """

        # Convert ChatCompletionMessage objects to dicts if they exist
        if any([isinstance(m, ChatCompletionMessage) for m in messages]):
            messages = list(map(lambda x: x.to_dict(), messages))

        request_body = {
            'model': model,
            'messages': messages,
            'temperature': temperature,
            'n': n,
            'stop': stop,
            'presence_penalty': presence_penalty,
            'frequency_penalty': frequency_penalty
        }
        # Note need to add max_tokens seperately because if not using it must be
        # left out of request body
        if max_tokens:
            request_body['max_tokens'] = max_tokens

        headers = {**self.headers, **self.header_content_type}
        response_json = self.request(
            'POST', f'/chat/completions', request_body=request_body, headers=headers
        )
        choices = response_json['choices']
        if len(choices) > 1:
            logger.warning(
                f"More than one choices returned, taking first "
                f"choice. Number of choices {len(choices)}."
            )
        model_chat_completion = choices[0]['message']['content']

        return model_chat_completion

    def create_completion(
            self,
            prompt: Union[str, List[str]],
            model: str = 'text-davinci-003',
            temperature: float = 1.0,
            n: int = 1,
            stop: Optional[int] = None,
            max_tokens: Optional[int] = None,  # openai by default sets this to inf
            presence_penalty: float = 0.0,
            frequency_penalty: float = 0.0,
            best_of: int = 1
    ) -> dict:
        """
        Creates a completion for the provided prompt and parameters.

        Example request body:
        {
          "model": "text-davinci-003",
          "prompt": "Say this is a test",
          "max_tokens": 7,
          "temperature": 0,
          "top_p": 1,
          "n": 1,
          "stream": false,
          "logprobs": null,
          "stop": "\n"
        }

        Example response:
        {
          "id": "cmpl-uqkvlQyYK7bGYrRHQ0eXlWi7",
          "object": "text_completion",
          "created": 1589478378,
          "model": "text-davinci-003",
          "choices": [
            {
              "text": "\n\nThis is indeed a test",
              "index": 0,
              "logprobs": null,
              "finish_reason": "length"
            }
          ],
          "usage": {
            "prompt_tokens": 5,
            "completion_tokens": 7,
            "total_tokens": 12
          }
        }

        It is possible to use the openai package to create completion:

        import os
        import openai
        openai.api_key = os.getenv("OPENAI_API_KEY")
        openai.Completion.create(
          model="text-davinci-003",
          prompt="Say this is a test",
          max_tokens=7,
          temperature=0
        )

        Args:
            prompt: The prompt(s) to generate completions for, encoded as a
                string, array of strings, array of tokens, or array of
                token arrays.
            model: ID of the model to use. See the model endpoint
                compatibility table for details on which models work with
                the Chat API.
            temperature: What sampling temperature to use, between 0 and 2.
                Higher values like 0.8 will make the output more random,
                while lower values like 0.2 will make it more focused and
                deterministic.We generally recommend altering this or top_p
                but not both.
            n: How many chat completion choices to generate for each input
                message.
            stop: Up to 4 sequences where the API will stop generating
                further tokens.
            max_tokens: The maximum number of tokens to generate in the
                chat completion. The total length of input tokens and
                generated tokens is limited by the model's context length.
            presence_penalty: Number between -2.0 and 2.0. Positive values
                penalize new tokens based on whether they appear in the
                text so far, increasing the model's likelihood to talk
                about new topics.
            frequency_penalty: Number between -2.0 and 2.0. Positive values
                penalize new tokens based on their existing frequency in
                the text so far, decreasing the model's likelihood to
                repeat the same line verbatim.
            best_of: Generates best_of completions server-side and returns
                the "best" (the one with the highest log probability per
                token). Results cannot be streamed. When used with n,
                best_of controls the number of candidate completions and n
                specifies how many to return – best_of must be greater than
                n. Note: Because this parameter generates many completions,
                it can quickly consume your token quota. Use carefully and
                ensure that you have reasonable settings for max_tokens and
                stop.

        Returns:
            model_completion: response from ChatGPT completion
        """
        request_body = {
            "model": model,
            "prompt": prompt,
            "temperature": temperature,
            "n": n,
            "stop": stop,
            "presence_penalty": presence_penalty,
            "frequency_penalty": frequency_penalty,
            "best_of": best_of
        }
        if max_tokens:
            request_body['max_tokens'] = max_tokens

        headers = {**self.headers, **self.header_content_type}
        response_json = self.request(
            'POST', f'/completions', request_body=request_body, headers=headers
        )
        choices = response_json['choices']
        if len(choices) > 1:
            logger.warning(
                f"More than one choices returned, taking first "
                f"choice. Number of choices {len(choices)}"
            )
        model_completion = choices[0]['text']

        return model_completion

    def create_edits(
            self,
            instruction: str,
            input: str = '',
            model: str = 'text-davinci-edit-001',
            temperature: float = 1.0,
            n: int = 1,
    ) -> dict:
        """
        Creates a new edit for the provided input, instruction, and parameters.

        Example request body:
        {
          "model": "text-davinci-edit-001",
          "input": "What day of the wek is it?",
          "instruction": "Fix the spelling mistakes"
        }

        Example response:
        {
          "object": "edit",
          "created": 1589478378,
          "choices": [
            {
              "text": "What day of the week is it?",
              "index": 0,
            }
          ],
          "usage": {
            "prompt_tokens": 25,
            "completion_tokens": 32,
            "total_tokens": 57
          }
        }

        It is possible to use the openai package to create completion:

        import os
        import openai
        openai.api_key = os.getenv("OPENAI_API_KEY")
        openai.Edit.create(
          model="text-davinci-edit-001",
          input="What day of the wek is it?",
          instruction="Fix the spelling mistakes"
        )

        Args:
            instruction: The instruction that tells the model how to edit
                the prompt.
            input: The input text to use as a starting point for the edit.
            model: ID of the model to use.
            temperature: What sampling temperature to use, between 0 and 2.
                Higher values like 0.8 will make the output more random,
                while lower values like 0.2 will make it more focused and
                deterministic.We generally recommend altering this or top_p
                but not both.
            n: How many chat completion choices to generate for each input
                message.

        Returns:
            model_edits: response from ChatGPT edits
        """
        request_body = {
            'model': model,
            'input': input,
            'instruction': instruction,
            'temperature': temperature,
            'n': n
        }

        headers = {**self.headers, **self.header_content_type}
        response_json = self.request('POST', f'/edits', request_body=request_body, headers=headers)
        choices = response_json['choices']
        if len(choices) > 1:
            logger.warning(
                f"More than one choices returned, taking first "
                f"choice. Number of choices {len(choices)}"
            )
        model_edits = choices[0]['text']

        return model_edits


if __name__ == '__main__':
    model = ChatGPTClient()
    models = model.list_models()
