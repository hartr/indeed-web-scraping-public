"""Classes used to manage the job search."""
import os
import re
import logging
import time
import pandas as pd
from datetime import datetime
from typing import Optional, List
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By

import utils
from utils import Requirement

logging.basicConfig(level=utils.LOG_LEVEL)
logger = logging.getLogger(__name__)


class Job:
    """
    Job class to handle all job info scraped and processed.

    Developer notes: To add a new column to a dataframe/CSV you would need
    to add the column name constant to `utils`, add the attribute name to
    the Job constructor and map the column name to the attribute name in
    `retrieve_attribute`.

    This is a bit cumbersome and it may be cleaner to do the following:
    - Have the column name be the same as the Job attribute. This would
        allow for `retrieve_attribute` to be removed.
    - When it comes to saving the CSV, make the attribute name presentable,
        such as converting "skip_reason" to "Skip Reason".
    - Replace any use of the contants from `utils` in other modules with
        the attributes used by the Job class.
    - Create a new attribute in the Job constructor that is a list of all
        the columns in each CSV. I.e. move EXPECTED_COLUMNS_SAVED and
        EXPECTED_COLUMNS_PROCESSED to this class.
    """
    def __init__(self, card: WebElement, job_info: WebElement):
        self.card: WebElement = card
        self.job_info: WebElement = job_info
        self.str_to_num: dict = {'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5}
        self.regex: re.Pattern = re.compile(r'(\S+)\s+(\S+)\s+(\S+)\s+(year)', re.IGNORECASE)
        self.years_found: Optional[List[int]] = None
        self.mo: Optional[List[any]] = None

        # Job info that is saved to a CSV
        self.company: Optional[str] = None
        self.title: Optional[str] = None
        self.url: Optional[str] = None
        self.description: Optional[str] = None
        self.languages: Optional[str] = None
        self.responsibilities: Optional[str] = None
        self.skip_reason: Optional[str] = None

    def get_company_name(self):
        self.company = self.card.find_element(By.CSS_SELECTOR, 'span[data-testid="company-name"]').text

    def get_title(self):
        title_text = self.job_info.find_element(By.CLASS_NAME, 'jobsearch-JobInfoHeader-title-container ').text
        if title_text:
            self.title = title_text.split('\n')[0]

    def get_description(self):
        self.description = self.job_info.find_element(By.ID, 'jobDescriptionText').text

    def get_job_link(self):
        self.url = self.card.find_element(By.XPATH, ".//a[@href]").get_attribute('href')

    def bad_title(self, title: str):
        bad_title = True if title.lower() in self.title.lower() else False
        return bad_title

    def is_candidate_qualified(self, miny, maxy) -> bool:
        """Filter the years of experience and check if qualified."""

        def convert(string):
            try:
                return int(string.rstrip('+'))
            except ValueError:
                return None

        mo = self.regex.findall(self.description)
        years = []

        # Loop through each tuple found
        for s in mo:
            # Loop through each word of each tuple
            for w in s:
                # Try to convert string to integer
                if type(convert(w)) is int:
                    years.append(convert(w))
                # If string such as '1-2' is encountered
                elif '-' in w:
                    split = w.split('-')
                    for ww in split:
                        if type(convert(ww)) is int:
                            years.append(convert(ww))
                # Check if tuple contains numbers as words Eg 'one', 'two', etc
                elif w.rstrip('+') in self.str_to_num.keys():
                    years.append(self.str_to_num[w.rstrip('+')])

        # Remove any duplicate years
        years = list(set(years))

        # Remove max years over 15. This may be needed because a company might
        # mention they've been in the business for 30+ years
        while len(years) != 0 and max(years) > 15:
            index = years.index(max(years))
            years.pop(index)

        # Applicant qualified if no years were found or if years fall within the desired range
        is_qualified = True if years == [] or (min(years) >= miny and max(years) <= maxy) else False

        self.mo = mo
        self.years_found = years
        return is_qualified

    def has_required_keywords(self, keywords: list, requirement: Requirement = Requirement.ANY) -> bool:
        """
        Check if keywords are in job description -- Default is to check if
        ANY of the keywords exist in the description.
        """
        results = []
        for word in keywords:
            r = True if word in self.description else False
            results.append(r)
        if requirement == Requirement.ANY:
            has_required_keywords = any(results)
        elif requirement == Requirement.ALL:
            has_required_keywords = all(results)
        return has_required_keywords

    def collect_data_for_df(self, columns: list) -> list:
        """Get list of job data based on columns in dataframe."""
        data = []
        for c in columns:
            data.append(self.retrieve_attribute(c))
        return data

    def retrieve_attribute(self, a):
        """Map of Job class attributes to the column names of the CSVs."""
        if a == utils.JOB_COMPANY:
            return self.company
        elif a == utils.JOB_TITLE:
            return self.title
        elif a == utils.JOB_URL:
            return self.url
        elif a == utils.JOB_LANGUAGES:
            return self.languages
        elif a == utils.JOB_DATE_SAVED:
            return pd.to_datetime(datetime.now())
        elif a == utils.JOB_APPLIED_DATE:
            return ''
        elif a == utils.JOB_STATUS:
            return 'Not Applied'
        elif a == utils.JOB_NOTES:
            return ''
        elif a == utils.JOB_RESPONSIBILITIES:
            return self.responsibilities
        elif a == utils.JOB_DESCRIPTION:
            return self.description
        elif a == utils.JOB_SKIP_REASON:
            return self.skip_reason
        else:
            KeyError(f'no attribute defined for {a}')


class RecordDF:
    """Store job info in local CSVs."""
    def __init__(
            self,
            saved_jobs_path: str,
            col_names: list[str],
            parse_dates: Optional[List[str]] = None,
            overwrite: bool = True
    ):
        self.saved_jobs_path = saved_jobs_path
        self.col_names = col_names
        self.parse_dates = parse_dates
        self.overwrite = overwrite
        self.df = self.load_csv()
        self.new_job_count = 0

    def add_job_to_df(self, job_data: list):
        """Add job data to end of dataframe."""
        self.df.loc[len(self.df)] = job_data
        self.new_job_count += 1

    def sort_by_column(self, column: List[str]):
        self.df.sort_values(column, ascending=False, inplace=True)

    def load_csv(self) -> pd.DataFrame:
        """Try to load existing record CSV or create a new one."""
        if os.path.exists(self.saved_jobs_path):
            logger.info(f'Opening existing record (csv) {self.saved_jobs_path}')
            df = pd.read_csv(self.saved_jobs_path, usecols=self.col_names, parse_dates=self.parse_dates)
        else:
            logger.info(f'Creating a new record (csv) {self.saved_jobs_path}')
            df = pd.DataFrame(columns=self.col_names)
        return df

    def save_csv(self, filepath: Optional[str] = None):
        if not filepath:
            filepath = self.saved_jobs_path
        if os.path.exists(filepath) and not self.overwrite:
            filepath = filepath.rstrip('.csv') + str(int(time.time())) + '.csv'
        logger.info(f'Saving CSV to {filepath}')
        self.df.to_csv(filepath, index=False)
