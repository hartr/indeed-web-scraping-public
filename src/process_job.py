"""Scrape job information and run a series of checks to see if user is qualified."""
import time
import logging
from random import randint
from typing import Optional, List, Union
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from utils import (
    wait,
    user_fixes_driver_failure,
    basic_check_for_processed_job,
    advanced_check_for_saved_job,
    WEB_DRIVER_WAIT_SEC,
    JobSearchParams,
    RunSettings,
    LOG_LEVEL
)
from classes import Job, RecordDF
from chatgpt import ChatGPTClient, ChatCompletionMessage

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)


def get_job_cards(driver: WebDriver) -> Union[List[WebElement], str]:
    """Get all the job cards on the left side of the page."""
    start = time.time()
    while True:
        try:
            cards = driver.find_element(By.XPATH, "//ul[contains(@class, 'css-zu9cdh eu4oa1w0')]").\
                find_elements(By.XPATH, "//div[contains(@class, 'slider_container')]")
            break
        except NoSuchElementException as e:
            if time.time() - start > WEB_DRIVER_WAIT_SEC:
                user_input = user_fixes_driver_failure(e, 'get_job_cards')
                if user_input in ['skip', 'retry']:
                    return user_input
            wait()
        except ElementClickInterceptedException:
            logger.error('message box caused ElementClickInterceptedException')
            user_input = input(
                'Exit info box and then finish search - Click desired job age, job distance, if '
                'remote position, desired page number, and then press Enter.'
            )
    return cards


def get_job_info(driver: WebDriver) -> Union[List[WebElement], str]:
    """Get job info on right pane."""
    start = time.time()
    while True:
        try:
            job_info = driver.find_element(By.ID, 'jobsearch-ViewjobPaneWrapper')
            break
        except NoSuchElementException as e:
            if time.time() - start > WEB_DRIVER_WAIT_SEC:
                user_input = user_fixes_driver_failure(e, 'get_job_info')
                if user_input in ['skip', 'retry']:
                    return user_input
                continue
            wait()
    return job_info


def ask_chatgpt_if_qualified(chatgpt: ChatGPTClient, max_years: int, resume: str) -> int:
    """
    Provide the job description and users resume to check if ChatGPT thinks
    user us qualified.

    Note: this function has not been tested thoroughly

    Args:
        chatgpt: OpenAI client that works with ChatGPT
        max_years: Max years qualified for the position
        resume: Your resume read in as a string to povide to ChatGPT

    Returns:
        qualification_rating: a rating from 1 to 10 on how
            qualified user is according to ChatGPT
    """
    messages = chatgpt.message_constructor()
    messages.append(
        ChatCompletionMessage(role="user", content=(
            f"Given the job description in my last request, tell me if "
            f"I'm qualified for this job based on my resume. Please keep "
            f"in mind that I have {max_years} of experience as a software "
            f"developer. Please rate how qualified I am on a scale of 1 to 10, "
            f"10 for being a perfect match and 1 for having no relevant "
            f"experience. Your response should only include a integer. My "
            f"resume is the following:\n{resume}"
        ))
    )
    logger.info('ChatGPT request: rating my qualification for the position')
    qualification_rating = chatgpt.create_chat_completion(messages)

    # Try to convert ChatGPT response to an int
    try:
        qualification_rating = int(qualification_rating)
    except ValueError:
        raise ValueError(
            f'Could not convert ChatGPT response to a integer rating {qualification_rating}'
        )

    return qualification_rating


def ask_chatgpt_to_highlight_job_responsibilities(chatgpt: ChatGPTClient, description: str) -> str:
    """
    Ask ChatGPT to give top three highlights of the job description.

    Args:
        chatgpt: OpenAI client that works with ChatGPT
        description: job description

    Returns:
        response from ChatGPT
    """
    messages = chatgpt.message_constructor()
    messages.append(
        ChatCompletionMessage(role="user", content=(
            f"Given this job description, highlight the 3 most "
            f"important responsibilities\n\n{description}"
        ))
    )
    logger.info('ChatGPT request: highlighting top three job responsibilities')
    return chatgpt.create_chat_completion(messages)


def ask_chatgpt_to_identify_software_tools(chatgpt: ChatGPTClient, description: str) -> str:
    """
    Ask ChatGPT to identify the software tools listed in the job description.

    Args:
        chatgpt: OpenAI client that works with ChatGPT
        description: job description

    Returns:
        response from ChatGPT
    """
    messages = chatgpt.message_constructor()
    messages.append(
        ChatCompletionMessage(role="user", content=(
            f"Given this job description, give me a list of software "
            f"tools used for this job. For example, a software tool may "
            f"be a computer language like Python or it may be more broad "
            f"like Azure or Kubernetes. Your response should be just the "
            f"list, for example: \"Python, GNU, Groovy\". If you do not "
            f"find any software tools in the job description do not try "
            f"to guess what software tools are used and respond with "
            f"\"None\"\n{description}"
        ))
    )
    logger.info('ChatGPT request: identify software tools')
    return chatgpt.create_chat_completion(messages)


def skip_job(job: Job, processed_rdf: RecordDF, skip_reason: str):
    logger.info(f'SKIPPING {job.title} - {skip_reason}')
    job.skip_reason = skip_reason
    processed_rdf.add_job_to_df(job.collect_data_for_df(processed_rdf.col_names))


def analyze_jobs(
        driver: WebDriver,
        job_search_params: JobSearchParams,
        run_settings: RunSettings,
        saved_rdf: RecordDF,
        processed_rdf: RecordDF,
        resume: Optional[str] = None
) -> Union[str, int]:
    """
    Scrape job information and run a series of checks to see if user is
    qualified.

    The are a series of checks that occur and if a job fails those checks
    then `continue` to the next job (i.e. don't save job). The last few
    steps use ChatGPT to analyze the job description and save the job info
    to saved_jobs_path. The process follows:
    1. Get all the job cards on the left side of the page. Loop through
        each job card.
    2. Skip job if it has already been saved in processed_jobs or saved_jobs
    3. OPTIONAL - Skip card if undesirable job title is found
    4. OPTIONAL - Skip job if desired keywords are not included in the job
        description
    5a. If you are using your resume, use ChatGPT to decide if you're
        qualified based on your years of experience and your resume
    5b. Otherwise, use the legacy algorithm to decide if your qualified.
        Basically uses a regex search to find references of "years" in the
        job descrition and checks if your min_years and max_years makes you
        qualified.
    6. OPTIONAL - Use ChatGPT to summarize the top three responsibilites of
        the job
    7. OPTIONAL - Use ChatGPT to collect data on softwarer tools
    8. Add job data to the two dataframes (saved and processed jobs)
    9. Try to go to the next page and repeat steps 2-8

    Args:
        driver: Selenium driver to scrape the world wide webbers
        job_search_params: config settings used to search for jobs
        run_settings: config settings used to run analyze_jobs
        saved_rdf: dataframe that has saved jobs to apply to
        processed_rdf: dataframe that hass all processed jobs
        resume: your resume

    Returns:
        'success': if the function completes without issue or the user
            enters 'skip' as an input
        integer: if the user enters 'retry' as an input, the integer
            represents the page number to retry search from
    """

    # Get current_page to return if program crashes
    try:
        current_page = int(driver.find_element(By.CSS_SELECTOR, 'button[data-testid="pagination-page-current"]').text)
    except NoSuchElementException:
        current_page = 1

    chatgpt = ChatGPTClient() if run_settings.use_chatgpt else None

    # Try to loop through each page and each job card until all pages are searched
    while True:

        cards = get_job_cards(driver)

        # If program craps out user has options to skip or retry search
        if cards == 'skip':
            logger.info('Skipping to next search with new job title and/or job location')
            break
        elif cards == 'retry':
            logger.info(f'Retrying current search from page {current_page}')
            return current_page

        # Loop through each job card
        for card in cards:

            wait(randint(0, 3))  # wait random seconds in an attempt to avoid CAPTCHA checks
            card.click()

            job_info = get_job_info(driver)

            # If program craps out user has options to skip or retry search
            if job_info == 'skip':
                logger.info('Skipping to next page of current search')
                break
            elif job_info == 'retry':
                logger.info(f'Retrying current search from page {current_page}')
                return current_page

            # Create job object and extract job info from HTML
            job = Job(card, job_info)
            job.get_company_name()
            job.get_title()
            job.get_job_link()
            job.get_description()

            # Skip job if unable to get description
            if not job.description:
                logger.warning(f'SKIPPING {job.title} - failed to get job description')
                continue

            # Skip job if it has already been saved in the processed_jobs csv
            if basic_check_for_processed_job(processed_rdf.df, job.url, job.title):
                logger.info(f'SKIPPING {job.title} - job already processed')
                continue

            # Skip job if it has already been saved in the saved_jobs csv
            if advanced_check_for_saved_job(saved_rdf.df, job.company, job.title, job.description):
                logger.info(f'SKIPPING {job.title} - similar job found in saved jobs')
                continue

            # Skip job if undesirable job title is found
            if job_search_params.avoid_titles:
                title_check = [job.bad_title(title) for title in job_search_params.avoid_titles]
                if any(title_check):
                    skip_reason = 'title check fail'
                    skip_job(job, processed_rdf, skip_reason)
                    continue

            # Skip job if desired keywords are not included in the job description
            if job_search_params.required_keywords and \
                    not job.has_required_keywords(job_search_params.required_keywords):
                skip_reason = (
                    f'job description does not have required keywords '
                    f'{job_search_params.required_keywords}'
                )
                skip_job(job, processed_rdf, skip_reason)
                continue

            # Use ChatGPT to decide if user is qualified based on user's years of experience resume.
            # ChatGPT should response with a rating of 1 to 10, 10 for being a perfect match and 1
            # for having no relevant experience. Scores with a 5 or higher will be saved. This
            # code path has not been tested thoroughly.
            if resume:
                qualification_rating = ask_chatgpt_if_qualified(
                    chatgpt, job_search_params.max_years, resume
                )

                # Check if qualified
                if 5 <= int(qualification_rating) <= 10:
                    pass
                elif 1 <= int(qualification_rating) < 5:
                    skip_reason = f'not qualified according to ChatGPT. ChatGPT rating: {qualification_rating}'
                    skip_job(job, processed_rdf, skip_reason)
                    continue
                else:
                    raise RuntimeError(f"Unexpected response: {qualification_rating}")

            # Otherwise use the legacy algorithm to decide if qualified. Algorith performs a regex
            # search on the job description for any references of years, and then checks if
            # min_years and max_years of experience makes user qualified.
            else:
                if not job.is_candidate_qualified(job_search_params.min_years, job_search_params.max_years):
                    skip_reason = (
                        f"not enough years experience: [{job_search_params.min_years}, "
                        f"{job_search_params.max_years}] years. Regex matched objects "
                        f"found: {[' '.join(group) for group in job.mo]}"
                    )
                    skip_job(job, processed_rdf, skip_reason)
                    continue

            # ########################################################
            # At this point the job is being saved in saved_jobs_path,
            # do further analysis of the job using ChatGPT
            # ########################################################

            # Use ChatGPT to summarize the top three responsibilites of the job
            if run_settings.use_chatgpt:
                job.responsibilities = ask_chatgpt_to_highlight_job_responsibilities(chatgpt, job.description)

            # Use ChatGPT to collect data on software tools
            if run_settings.identify_software_tools:
                job.languages = ask_chatgpt_to_identify_software_tools(chatgpt, job.description)

            logger.info(f'SAVING {job.title}')
            processed_rdf.add_job_to_df(job.collect_data_for_df(processed_rdf.col_names))
            saved_rdf.add_job_to_df(job.collect_data_for_df(saved_rdf.col_names))

        # Try to go to the next page if it exists
        try:
            current_page += 1
            next_page = driver.find_element(By.XPATH, "//nav[contains(@class, 'css-jbuxu0 ecydgvn0')]").find_element(By.LINK_TEXT, str(current_page))
            next_page.click()
            logger.info(f'Navigated to page {current_page}')
        except NoSuchElementException:
            break

    return "success"
