#!/bin/bash
# If you are using a Mac, you can use this script to run multiple job searches in parallel, e.g. search for in person
# jobs locally and search for remote positions in multiple cities

echo "Starting first run_assistant"
osascript -e 'tell app "Terminal" to do script "conda activate job-assistant && cd /path/to/job-application-assistant/src && export CONFIG_PATH=/path/to/job-application-assistant/job_search_config_remote.json && python /path/to/job-application-assistant/src/run_assistant.py"' &

sleep 10

echo "Starting second run_assistant"
osascript -e 'tell app "Terminal" to do script "conda activate job-assistant && cd /path/to/job-application-assistant/src && export CONFIG_PATH=/path/to/job-application-assistant/job_search_config.json && python /path/to/job-application-assistant/src/run_assistant.py"' &

wait # Wait for background jobs to finish
echo "Finished running run_assistant in parallel"