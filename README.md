# Job Application Assistant

These scripts scrape Indeed.com for job postings and process the job info through a ChatGPT client 
(optional) to find jobs you're qualified for. 

For a video tutorial of how to use this repository, check out this [video](https://www.youtube.com/watch?v=_3-lv3w0H90).

## Installation

You can find the packages required in the `requirements.txt`

Using Selenium requires a driver to interface with the chosen browser. `run_assistant.py` is set up 
to use [geckodriver](https://github.com/mozilla/geckodriver/releases) for Firefox. 

---

Installation is straightforward using Conda. 
```
conda create -c conda-forge -n job-assistant -y python pip geckodriver nltk pandas requests scikit-learn selenium
conda activate job-assistant
```

---

If using pip, remove `geckodriver` from `requirements.txt` and run:
```
pip install -r requirements.txt
```

Then download the geckodriver and place the executable in your PATH. You can follow
the instructions below:
1. Download the geckodriver [here](https://github.com/mozilla/geckodriver/releases) 
2. Unzip it
3. Copy that .exe file and put your into python parent folder (e.g., C:\Python34)

See [Stack Overflow](https://stackoverflow.com/questions/41190989/how-do-i-install-geckodriver) 
for more information.

---

The `ChatGPTClient` is currently set up to use `requests` to communicate with OpenAI/ChatGPT. As 
described in the docstrings of the `chatgpt` module, you can also use the `openai` package to use 
ChatGPT in the same way. To install the `openai` package with pip use:
```
pip install -r requirements.txt --extras optional
```

## Usage

To run these scripts, first complete the `job_search_config.json`. This contains all the settings 
to search for jobs. You can see what settings are available, and what defaults are provided, by 
checking the `JobSearchParams` and `RunSettings` dataclasses in the `utils` module. 

There are up to two environment variables to set:
- `CONFIG_PATH` - optional - defaults to './job-application-assistant/job_search_config.json
- `OPENAI_KEY` - optional - only required if using OpenAI, go to their site to get a key

___

To run the scripts:
1. Open a terminal and activate your Conda or pip environment
2. Change directory to `path/to/job-application-assistant/src`
3. Run `python run_assistant.py` or open up your IDE and run `run_assistant`

For more information on how these scripts work, check out the docstring of `run_assistant` and 
`process_job.analyze_jobs`.

___

There are a multiple `input` statements to accept user input to handle cases when the scripts don't 
run perfectly. 

A few of cases are:
- When the Selenium driver fails to select your search options: job age, distance, etc.
- When Indeed flags the Selenium driver scraping its site and presents a CAPTCHA checkpoint.
- When Indeed (for some unknown reason) loads a single job description when looping through job cards
- When a 502 error occurs
- Plus other cases

There are helpful prompts to help navigate you to continue on with the program if an issue occurs.

## ChatGPT

The ChatGPT client uses HTTP requests to communicate with ChatGPT. You will need to go to OpenAI's 
website to get a authentication key in order to make requests (you'll need a credit card). The 
`chatgpt` module is designed to be a stand alone module that can be imported by itself to make 
requests to ChatGPT.

## Releases

The site Indeed continues to change and `job-application-assistant` continues to need updates. 
I create a new branch each time there are major changes to these scripts.

- `main_v01`
  - Logs into your indeed account and saves jobs directly on your account.
- `main_v02`
  - Does not log into your account but instead saves jobs to a CSV.
  - Updates to HTML elements to allow for scraping
  - Changes to how jobs are searched for
  - Can search by distance now
  - Improved logging
  - New CSV Record class
  - Archiecture changes to allow for better division of object roles
- `main_v03`
  - Refactored repository into distinct modules
  - Now uses ChatGPT to process the job description and your resume to determine if applicant is 
  qualified -- optional, need to set up OpenAI key
  - Removed sign in functionality
- `main_v04`
  - General refactoring, e.g. moving and organizing functions without changing functionality
  - Maintenance to HTML/XPATH in driver searches
  - New search functionality:
    - Search for jobs in multiple locations
    - Search for remote positions
    - Start search from particular page
  - Added retry and skip functionality. In event a issue stemming from Indeed, driver, etc occurs, 
  user now has the option to retry or skip a search.
  - Navigation includes new wait-for-button-to-appear functionality and pop-up window handling
  - Much better handling of driver failures, e.g. window doesn't load correctly. More instances
  where user inputs option to allow program to continue
  - Advanced search of whether job had been processed already using nltk and cosine similarity. 
  This was needed because the same job, i.e. matching job title and description, would be saved 
  twice because the job links were different.
  - Fixed a couple bugs, including one that prevented new jobs from being saved correctly
  - New resume_assitant that takes a job description and your resume and creates new resume content
  to update your resume
  - New run_assistant_parallel.sh to run run_assitant, works for Mac users
  - New functionality to get past issue of Indeed loading single job page when looping through job
  cards
  - Updated data persisted to saved_jobs and processed_jobs CSVs, i.e. different columns


## Contact

If you have any questions please contact me through Medium or through GitLab. Thanks for checking 
this project out!
